# wtf

A (w)rap (t)erra(f)orm script to manage a profile that you can source with other
aws profiles and get more life out of your MFA tokens. The profiles are easy to
use by shell scripts too.

It is meant to be transparent until the session token expires, which then will
prompt you for an MFA token before passing along the rest of the args to
terraform.

# why

A bit simpler than the (very useful an more featureful) [assume-role][1]

## Setup

### Requirements

Bash... ;)

### Installation

Get this script onto your PATH. Place it in or link to it from in somewhere
like $HOME/bin or /usr/local/bin.

### Initialize

If the backend configuration for Terraform has changed ensure you reinitialize
with:

    wtf init

You can also upgrade your aws provider (older versions might not play as nice):

    wtf init --upgrade=true

It's also worth noting that sometimes Terraform will simply refuse to
reinitialize. If that's the case, and you know what you're doing and are
POSITIVE you won't blame anybody but yourself delete the projects .terraform
directory and try the init step again.

### Profiles

`wtf` assumes a certain profile naming convention.

- The `default` profile is your main AWS account that has an MFA device arn
  associated with it.
- A `wtf` profile will be created/managed and used solely by the script to hold
  the session credentials.
- Other roles should source from `wtf` so that MFA auths happen less
  frequently.

In practice I name my profiles by ORG-ACCOUNT-ROLE. Example `~/.aws`
config/creds using different role names:

    # ~/.aws/config

    [default]
    region = us-east-1
    output = text
    mfa_serial = < the arn of your MFA device for your aws account >

    [profile org-prod-admin]
    role_arn = arn:aws:iam::<prod-account-id>:role/Admin
    source_profile = wtf

    [profile org-prod-developer]
    role_arn = arn:aws:iam::<prod-account-id>:role/Dev
    source_profile = wtf

    [profile org-dev-admin]
    role_arn = arn:aws:iam::<dev-account-id>:role/Admin
    source_profile = wtf

    ... and so on ...


    # ~/.aws/credentials

    [default]
    aws_access_key_id = ...
    aws_secret_access_key = ...

Credentials for the `wtf` role will be handled by the script.

## Usage

### Example

    $ wtf init
    MFA token: 123456

    Initializing the backend...

### Terraform Code

Backends and providers:

    terraform {
      backend "s3" {
        profile = "org-prod-admin"
        ... (region, key, bucket, etc)
      }
    }

    provider "aws" {
      profile = "org-prod-developer"
      ...
    }

    provider "aws" {
      profile = "org-dev-admin"
      alias = "dev"
      ...
    }

### Shell scripts

To refresh the wtf token run the command `wtf` with no arguments. This will
refresh the profile credentials and allow you to seamlessly switch into new
profiles.

To use the profiles it is not neccessary to assume roles or fetch STS tokens,
just use the profile. Here's an example S3 call:

    AWS_PROFILE=org-prod-admin aws s3 ls

And in a different account or role:

    AWS_PROFILE=org-dev-admin aws s3 ls


[1]: https://github.com/coinbase/assume-role
